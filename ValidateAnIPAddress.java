//{ Driver Code Starts
// Initial Template for Java

// Initial Template for Java

import java.util.*;
import java.io.*;

  public class validip {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();

        while (t-- > 0) {
            String s = sc.next();
            Solution obj = new Solution();

            if (obj.isValid(s))
                System.out.println(1);
            else
                System.out.println(0);
        }
    }
}
// } Driver Code Ends


class Solution {

    public boolean isValid(String s) {
        // Split the input string by '.'
        String[] parts = s.split("\\.");

        // If the length of parts is not 4, it's not a valid IPv4 address
        if (parts.length != 4)
            return false;

        for (String part : parts) {
            // If the part is empty or has leading zeroes, it's not valid
            if (part.isEmpty() || (part.length() > 1 && part.charAt(0) == '0'))
                return false;

            try {
                // Convert the part to an integer
                int num = Integer.parseInt(part);

                // If the number is not in the range [0, 255], it's not valid
                if (num < 0 || num > 255)
                    return false;
            } catch (NumberFormatException e) {
                // If the part is not a valid integer, it's not valid
                return false;
            }
        }

        // Check if the input string ends with a dot ('.')
        if (s.charAt(s.length() - 1) == '.')
            return false;

        return true;
    }
}
